﻿using System;
using System.Collections.Generic;

namespace Cache
{
    public class LRUImplementation :ICache

    {
        private int capacity;
        private int count;
        Dictionary<int, LRUNode> dicLruNodes;
        DoubleLinkedList doubleLinkedList;

        public LRUImplementation(int capacity)
        {
            this.capacity = capacity;
            this.count = 0;
            dicLruNodes = new Dictionary<int, LRUNode>();
            doubleLinkedList = new DoubleLinkedList();
        }
        // each time when access the node, node is moving to the top
        public int Get(int key)
        {
           
                if (!dicLruNodes.ContainsKey(key)) return -1;
                var node = dicLruNodes[key];
                doubleLinkedList.RemoveNode(node);
                doubleLinkedList.AddToTop(node);
                return node.Value;
        }

        public void Add(int key, int value)
        {
            // update the value and move it to the top
            if (dicLruNodes.ContainsKey(key))
            {
                LRUNode node = dicLruNodes[key];
                doubleLinkedList.RemoveNode(node);
                node.Value = value;
                doubleLinkedList.AddToTop(node);
            }
            else
            {
                // if cache is full, then remove the least recently used node
                if (count==capacity) {
                    var lru = doubleLinkedList.RemoveLruNode();
                    dicLruNodes.Remove(lru.Key);
                    count--;
                }

                // add a new node
                var node = new LRUNode(key, value);
                doubleLinkedList.AddToTop(node);
                dicLruNodes[key] = node;
                count++;
            }
        }
    }
}