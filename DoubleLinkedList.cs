﻿using System.Security.Cryptography.X509Certificates;

namespace Cache
{
    public class DoubleLinkedList
    {
        private LRUNode Head;
        private LRUNode Tail;

        public DoubleLinkedList()
        {
            Head = new LRUNode();
            Tail = new LRUNode();
            Head.Next = Tail;
            Tail.Previous = Head;
            
        }
        //add a new node to the top of the double-linked list
        public void AddToTop(LRUNode node)
        {
            node.Next = Head.Next;
            Head.Next.Previous = node;
            node.Previous = Head;
            Head.Next = node;
        }
        
        //remove a node from double-linked list
        public void RemoveNode(LRUNode node)
        {
            node.Previous.Next = node.Next;
            node.Next.Previous = node.Previous;
            node.Next = null;
            node.Previous = null;
        }
        //remove the least recently used node from double-linked list
        public LRUNode RemoveLruNode()
        {
            LRUNode target = Tail.Previous;
            RemoveNode(target);
            return target;
        }
    }
}